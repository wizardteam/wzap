#include "wzap.h"
#include "EffectBundle.h"

static ap_wrapper_t *g_ap;
static int wzap_inited = 0;
static wzap_lock_t mutex;

#define EQ_ANDROID 1

static void reg_ap_all()
{
#if EQ_ANDROID
    extern ap_wrapper_t ap_lvm_eq;
    g_ap = &ap_lvm_eq;
#else
    extern ap_wrapper_t ap_private;
    g_ap = &ap_private;
#endif
}

static int wzap_select(wzap_context_t *ctx)
{
    if(!g_ap)
        return -1;
    ctx->wrapper = g_ap;
    return 0;
}

void wzap_global_init()
{
    if(wzap_inited == 1)
        return;
    reg_ap_all();
    wzap_inited = 1;
    return;
}

int wzap_init(wzap_context_t *ctx)
{
    int ret = 0;
    wzap_global_init();
    ret = wzap_select(ctx);
    if(ret < 0)
        goto EXIT;
    ap_wrapper_t *wrapper = ctx->wrapper;
    ret = wrapper->init(ctx);
    wzap_print("wzap_init ok \n");
    ctx->inited = 1;
    wzap_lock_init(&mutex, NULL);
EXIT:
    return ret;
}

int wzap_process(wzap_context_t *ctx, wzap_frame_t *frame)
{

    if(!ctx->inited)
        wzap_init(ctx);
    wzap_lock(&mutex); 
    ap_wrapper_t *wrapper = ctx->wrapper;
    wrapper->process(ctx, frame);
    wzap_unlock(&mutex); 
    return 0;
}

int wzap_reset(wzap_context_t *ctx, wzap_para_t *para)
{
    wzap_lock(&mutex); 
    ap_wrapper_t *wrapper = ctx->wrapper;
    wrapper->release(ctx);
    wzap_unlock(&mutex); 
    return 0;
}

int wzap_update(wzap_context_t *ctx)
{
    wzap_lock(&mutex); 
    ap_wrapper_t *wrapper = ctx->wrapper;
    wrapper->release(ctx);
    wzap_unlock(&mutex); 
    return 0;
}

int wzap_release(wzap_context_t * ctx)
{
    wzap_lock(&mutex); 
    wzap_print("wzap_release ok \n");
    wzap_unlock(&mutex); 
    return 0;
}
