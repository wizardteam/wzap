#ifndef WZAP_API_H
#define WZAP_API_H

#define MAX_NAME_LEN 1024

typedef enum{
    WZAP_ID_LVM     = 0x0,
    WZAP_ID_PRIVATE = 0x1
}wzap_id_t;

typedef enum{
    WZAP_EFFECT_EQ       = 0x0,
    WZAP_EFFECT_REVERB   = 0x1,
    
    WZAP_EFFECT_MAX      = 0x9,
}wzap_type_t;

typedef enum{
    //EQ PART
    EQ_EFFECT_NORMAL     = 0x00,
    EQ_EFFECT_CLASSICAL  = 0x01,
    EQ_EFFECT_DANCE      = 0x02,
    EQ_EFFECT_FLAT       = 0x03,
    EQ_EFFECT_FOLK       = 0x04,
    EQ_EFFECT_HEAVYMETAL = 0x05,
    EQ_EFFECT_HIPHOP     = 0x06,
    EQ_EFFECT_JAZZ       = 0x07,
    EQ_EFFECT_POP        = 0x08,
    EQ_EFFECT_ROCK       = 0x09,

    // REVERB
    REVERB_EFFECT_NONE   = 0x10,

    EQ_EFFECT_MAX        = 0x80
}wzap_item_t;

struct wzap_context;

typedef struct{
    uint8_t *in;
    int in_size;
}wzap_frame_t;

typedef struct{
    int samplerate;
    int channels;
    int data_width;
    wzap_type_t type;
    wzap_item_t item;
}wzap_para_t;

typedef struct{
    int id;
    int type;
    char *name;

    int (*init)     (struct wzap_context *ctx);
    int (*process)  (struct wzap_context *ctx, wzap_frame_t *frame);
    int (*config)   (struct wzap_context *ctx);
    int (*release)  (struct wzap_context *ctx);
}ap_wrapper_t;


/*
 * wzap_context_t 
 * every obj means one process context
 * multimle case support
 *
 * */
typedef struct wzap_context{
    wzap_para_t para;   // used to config ap
    char name[MAX_NAME_LEN];
    ap_wrapper_t *wrapper;
    uint8_t *out;
    int out_size;
    int inited;
    void *ap_priv;
}wzap_context_t;

int wzap_init(wzap_context_t *ctx);
int wzap_process(wzap_context_t *ctx, wzap_frame_t *frame);
int wzap_reset(wzap_context_t *ctx, wzap_para_t *para);
int wzap_update(wzap_context_t *ctx);
int wzap_release(wzap_context_t *ctx);

#endif
