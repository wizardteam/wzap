#include "wzap.h"

int bundle_test();

static void version_info()
{
    wzap_print("|===================================| \n"); 
    wzap_print("| version: wzap v1.0 \n"); 
    wzap_print("| build time "__DATE__ __TIME__" \n"); 
    wzap_print("|===================================| \n"); 
}

static void usage()
{
    wzap_print("|===================================| \n"); 
    wzap_print("| usage \n"); 
    wzap_print("| wzap in_file outfile \n"); 
    wzap_print("|===================================| \n"); 
}

static int feature_select(int *code)
{
    wzap_print("|===================================| \n"); 
    wzap_print("| select test item \n"); 
    wzap_print("| 1 EQ TEST \n"); 
    wzap_print("| 2 BUNDLE TEST \n"); 
    wzap_print("| 3 REVERB TEST \n"); 
    wzap_print("| 9 QUIT TEST \n"); 
    wzap_print("|===================================| \n");

    return scanf("%d]", code);
}

int main(int argc, char ** argv)
{
    version_info();

    if(argc < 3)
    {
        usage();
        return 0;
    }

    char *in = argv[1];
    char *out = argv[2];

    int test_code = -1;
    int ret = feature_select(&test_code);
    if(ret <= 0)
        return 0;

    while(test_code != 9)
    {
        if(test_code == 1)
            bundle_test(in, out);
        ret = feature_select(&test_code);
        if(ret <= 0)
            return 0;
    }
    return 0;
}
