#ifndef WZ_AP_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "wzap_api.h"

//################################################
#include "pthread.h"
#define wzap_lock_t         pthread_mutex_t
#define wzap_lock_init(x,v) pthread_mutex_init(x,v)
#define wzap_lock(x)        pthread_mutex_lock(x)
#define wzap_unlock(x)      pthread_mutex_unlock(x)
//################################################

//################################################
#define wzap_print printf

//################################################

//################################################
#define wzap_malloc malloc
#define wzap_free   free

//################################################

#endif
